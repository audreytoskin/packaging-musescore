# mscore

[MuseScore](https://musescore.org/) is a free cross platform WYSIWYG music
notation program.  Some highlights:

- WYSIWYG, notes are entered on a "virtual note sheet"
- Unlimited number of staves
- Up to four voices per staff
- Easy and fast note entry with mouse, keyboard or MIDI
- Integrated sequencer and FluidSynth software synthesizer
- Import and export of MusicXML and Standard MIDI Files (SMF)
- Translated in 26 languages
